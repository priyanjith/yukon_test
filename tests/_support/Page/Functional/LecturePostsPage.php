<?php
namespace Page\Functional;
use FunctionalTester;
class LecturePostsPage
{
    public static $url = '';
    public static function route($param)
    {
        return static::$url . $param;
    }
    public static $formFields = [
        'instructor_id' => 2,
        'subject_id' => 1,
        'start_time' => '2.30',
        'end_time' => '4.30',
        'recurrence' => 'WEEKLY',
    ];

    /**
     * @var FunctionalTester;
     */
    protected $tester;
    public function __construct(FunctionalTester $I)
    {
        $this->tester = $I;
    }

    public function createPost($fields = [])
    {
        $I = $this->tester;
        $I->amOnRoute('classes.create');
        $I->click('button[type=submit]');
        $this->fillFormFields($fields);
        $I->click('Submit');
    }

    public function editPost($id, $fields = [])
    {
        $I = $this->tester;
        $I->amOnPage(self::route("/classes/edit/$id"));
        $I->see('Recurrence', 'label');
        $this->fillFormFields($fields);
        $I->click('Submit');
    }

    public function deletePost($id)
    {
        $I = $this->tester;
        $I->amOnPage(self::route("/classes/delete/$id"));
    }

    protected function fillFormFields($data)
    {
        foreach ($data as $field => $value) {
            if (!isset(static::$formFields[$field])) {
                throw new \Exception("Form field  $field does not exist");
            }
            $this->tester->fillField($field, $value);
        }
    }
}