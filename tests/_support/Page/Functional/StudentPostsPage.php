<?php
namespace Page\Functional;
use FunctionalTester;
class StudentPostsPage
{
    public static $url = '';
    public static function route($param)
    {
        return static::$url . $param;
    }
    public static $formFields = [
        'first_name' => 'Test_first_name',
        'last_name' => 'Test_last_name',
        'grade' => '2D',
        'age' => 16,
        'description' => 'You are so awesome'
    ];

    /**
     * @var FunctionalTester;
     */
    protected $tester;
    public function __construct(FunctionalTester $I)
    {
        $this->tester = $I;
    }

    public function createPost($fields = [])
    {
        $I = $this->tester;
        $I->amOnRoute('students.create');
        $I->click('button[type=submit]');
        $this->fillFormFields($fields);
        $I->click('Submit');
    }

    public function editPost($id, $fields = [])
    {
        $I = $this->tester;
        $I->amOnPage(self::route("/students/edit/$id"));
        $I->see('First Name', 'label');
        $this->fillFormFields($fields);
        $I->click('Submit');
    }

    public function deletePost($id)
    {
        $I = $this->tester;
        $I->amOnPage(self::route("/students/delete/$id"));
    }

    protected function fillFormFields($data)
    {
        foreach ($data as $field => $value) {
            if (!isset(static::$formFields[$field])) {
                throw new \Exception("Form field  $field does not exist");
            }
            $this->tester->fillField($field, $value);
        }
    }
}