<?php
namespace Page\Functional;
use FunctionalTester;
class InstructorPostsPage
{
    public static $url = '';
    public static function route($param)
    {
        return static::$url . $param;
    }
    public static $formFields = [
        'full_name' => 'Test_full_name',
        'subject_id' => 1,
        'description' => 'You are so awesome'
    ];

    /**
     * @var FunctionalTester;
     */
    protected $tester;
    public function __construct(FunctionalTester $I)
    {
        $this->tester = $I;
    }

    public function CreatePost($fields = [])
    {
        $I = $this->tester;
        $I->amOnRoute('instructors.create');
        $I->click('button[type=submit]');
        $this->fillFormFields($fields);
        $I->click('Submit');
    }

    public function editPost($id, $fields = [])
    {
        $I = $this->tester;
        $I->amOnPage(self::route("/instructors/edit/$id"));
        $I->see('Full Name', 'label');
        $this->fillFormFields($fields);
        $I->click('Submit');
    }

    public function deletePost($id)
    {
        $I = $this->tester;
        $I->amOnPage(self::route("/instructors/delete/$id"));
    }

    protected function fillFormFields($data)
    {
        foreach ($data as $field => $value) {
            if (!isset(static::$formFields[$field])) {
                throw new \Exception("Form field  $field does not exist");
            }
            $this->tester->fillField($field, $value);
        }
    }
}