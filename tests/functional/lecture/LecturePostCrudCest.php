<?php
//use Page\Functional\LecturePostsPage;
//class LecturePostCrudCest
//{
//    private $postAttributes;
//    public function __construct()
//    {
//        $this->postAttributes = [
//            'instructor_id' => 1,
//            'subject_id' => 1,
//            'start_time' => '2.30',
//            'end_time' => '4.30',
//            'recurrence' => 'WEEKLY',
//            'created_at' => new DateTime(),
//            'updated_at' => new DateTime()
//        ];
//    }
//
//    // tests
//    public function lectureCreatePost(FunctionalTester $I, LecturePostsPage $postsPage)
//    {
//        $postsPage->createPost([
//            'instructor_id' => 1,
//            'subject_id' => 1,
//            'start_time' => '2.30',
//            'end_time' => '4.30',
//            'recurrence' => 'WEEKLY',
//        ]);
//        $I->amOnRoute('classes');
//        $I->see('Successfully');
//    }
//
//    public function lectureEditPost(FunctionalTester $I, LecturePostsPage $postsPage)
//    {
//
//        $id = $I->haveRecord('lectures', $this->postAttributes);
//        $postsPage->editPost($id, ['start_time' => '5.30']);
//        $I->amOnRoute('classes');
//        $I->see('Recurrence');
//
//    }
//
//    public function lectureDeletePost(FunctionalTester $I, LecturePostsPage $postsPage)
//    {
//        $id = $I->haveRecord('lectures', $this->postAttributes);
//        $I->amOnRoute('classes');
//        $I->see('MONTHLY');
//        $postsPage->deletePost($id);
//        $I->amOnRoute('classes');
//        $I->dontSee('Successfully');
//    }
//}