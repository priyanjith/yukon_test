<?php
use Page\Functional\InstructorPostsPage;
class InstructorPostCrudCest
{
    private $postAttributes;
    public function __construct()
    {
        $this->postAttributes = [
            'full_name' => 'Test_full_name',
            'subject_id' => 1,
            'description' => 'You are so awesome',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ];
    }

    // tests
    public function instructorCreatePost(FunctionalTester $I, InstructorPostsPage $postsPage)
    {
        $postsPage->createPost([
            'full_name' => 'Test_full_name',
            'description' => 'You are so awesome',
        ]);
        $I->amOnRoute('instructors');
        $I->see('Test_full_name');
    }

    public function instructorEditPost(FunctionalTester $I, InstructorPostsPage $postsPage)
    {
        $randName = "Test_first" . microtime();
        $id = $I->haveRecord('instructors', $this->postAttributes);
        $postsPage->editPost($id, ['full_name' => $randName]);
        $I->amOnRoute('instructors');
        $I->see($randName);
        $I->dontSee('Full Name', 'label');
    }

    public function instructorDeletePost(FunctionalTester $I, InstructorPostsPage $postsPage)
    {
        $id = $I->haveRecord('instructors', $this->postAttributes);
        $I->amOnRoute('instructors');
        $I->see('Test_full_name');
        $postsPage->deletePost($id);
        $I->amOnRoute('instructors');
        $I->dontSee('Test_full_name');
    }
}