<?php
use Page\Functional\StudentPostsPage;
class StudentPostCrudCest
{
    private $postAttributes;
    public function __construct()
    {
        $this->postAttributes = [
            'first_name' => 'Test_first_name',
            'last_name' => 'Test_last_name',
            'grade' => '2D',
            'age' => 16,
            'description' => 'You are so awesome',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ];
    }

    // tests
    public function studentCreatePost(FunctionalTester $I, StudentPostsPage $postsPage)
    {
        $postsPage->createPost([
            'first_name' => 'Test_first_name',
            'last_name' => 'Test_last_name',
            'grade' => '2D',
            'age' => 16,
            'description' => 'You are so awesome',
        ]);
        $I->amOnRoute('students');
        $I->see('Test_first_name');
    }

    public function studentEditPost(FunctionalTester $I, StudentPostsPage $postsPage)
    {
        $randName = "Test_first" . microtime();
        $id = $I->haveRecord('students', $this->postAttributes);
        $postsPage->editPost($id, ['first_name' => $randName]);
        $I->amOnRoute('students');
        $I->see($randName);
        $I->dontSee('First Name', 'label');
    }

    public function studentDeletePost(FunctionalTester $I, StudentPostsPage $postsPage)
    {
        $id = $I->haveRecord('students', $this->postAttributes);
        $I->amOnRoute('students');
        $I->see('Test_first_name');
        $postsPage->deletePost($id);
        $I->amOnRoute('students');
        $I->dontSee('Hello Universe');
    }
}