<?php
use App\Modules\Lectures\Models\Lecture;

class LectureTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {

    }

    function testSavingLecture()
    {
        $data = [
            'instructor_id' => 2,
            'subject_id' => 12,
            'start_time' => '12:35',
            'end_time' => '16:30',
            'recurrence' => 'WEEKLY',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ];
        $lecture = new Lecture();
        $lecture->create($data);
        $this->tester->seeRecord('lectures', ['recurrence'=> 'WEEKLY']);
    }

    function testEditLecture()
    {
        $id = $this->tester->haveRecord('lectures', [
            'instructor_id' => 2,
            'subject_id' => 1200,
            'start_time' => '12:35',
            'end_time' => '16:30',
            'recurrence' => 'WEEKLY',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        $lecture = Lecture::find($id);
        $lecture->subject_id = (1201);
        $lecture->save();
        $this->assertEquals(1201, $lecture->subject_id);
        $this->tester->seeRecord('lectures', ['subject_id' => 1201]);
        $this->tester->dontSeeRecord('lectures', ['subject_id' => 1200]);
    }
}
