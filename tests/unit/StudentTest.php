<?php
use App\Modules\Students\Models\Student;

class StudentTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {

    }

    function testSavingStudent()
    {
        $data = [
            'first_name' => 'Test_first_name',
            'last_name' => 'Test_last_name',
            'grade' => '2D',
            'age' => 16,
            'description' => 'You are so awesome',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ];
        $student = new Student();
        $student->create($data);
        $this->tester->seeRecord('students', ['first_name' => 'Test_first_name']);
    }

    function testEditStudent()
    {
        $id = $this->tester->haveRecord('students', [
            'first_name' => 'Test_first_name',
            'last_name' => 'Test_last_name',
            'grade' => '2D',
            'age' => 16,
            'description' => 'You are so awesome',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        $student = Student::find($id);
        $student->first_name = ('Edit_test_first_name');
        $student->save();
        $this->assertEquals('Edit_test_first_name', $student->first_name);
        $this->tester->seeRecord('students', ['first_name' => 'Edit_test_first_name']);
        $this->tester->dontSeeRecord('students', ['first_name' => 'Test_first_name']);
    }
}
