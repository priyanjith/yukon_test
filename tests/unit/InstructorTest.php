<?php
use App\Modules\Instructors\Models\Instructor;

class InstructorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {

    }

    function testSavingInstructor()
    {
        $data = [
            'full_name' => 'Test_full_name',
            'subject_id' => 16,
            'description' => 'You are so awesome',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ];
        $instructor = new Instructor();
        $instructor->create($data);
        $this->tester->seeRecord('instructors', ['full_name' => 'Test_full_name']);
    }

    function testEditInstructor()
    {
        $id = $this->tester->haveRecord('instructors', [
            'full_name' => 'Test_full_name',
            'subject_id' => 16,
            'description' => 'You are so awesome',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        $instructor = Instructor::find($id);
        $instructor->full_name = ('Edit_test_full_name');
        $instructor->save();
        $this->assertEquals('Edit_test_full_name', $instructor->full_name);
        $this->tester->seeRecord('instructors', ['full_name' => 'Edit_test_full_name']);
        $this->tester->dontSeeRecord('instructors', ['full_name' => 'Test_full_name']);
    }
}
