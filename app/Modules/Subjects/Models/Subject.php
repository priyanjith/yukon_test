<?php

namespace App\Modules\Subjects\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subjects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',  'syllabus'
    ];

    public function instructors()
    {
        return $this->hasMany('App\Modules\Instructors\Models\Instructor');
    }

    
}
