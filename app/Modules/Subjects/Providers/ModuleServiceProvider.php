<?php

namespace App\Modules\Subjects\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'subjects');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'subjects');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'subjects');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->bind('App\Modules\Subjects\Services\Interfaces\SubjectInterface', 'App\Modules\Subjects\Services\Repositories\SubjectRepository');
    }
}
