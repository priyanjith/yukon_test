<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();

        $names = array(
            1 => "English",
            2 => "Science",
            3 => "IT",
            4 => "Maths",
            5 => "Social Study",
            6 => "Art",
        );

        foreach ($names as $index => $name) {
            DB::table('subjects')->insert([
                'id'    => $index,
                'name'  => $name[$index],
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }
    }
}