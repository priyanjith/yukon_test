<?php

namespace App\Modules\Subjects\Services\Interfaces;

interface SubjectInterface
{
    public function getAllAsArray();
}

?>