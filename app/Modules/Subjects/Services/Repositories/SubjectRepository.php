<?php

namespace App\Modules\Subjects\Services\Repositories;

use App\Modules\Subjects\Models\Subject;
use App\Modules\Subjects\Services\Interfaces\SubjectInterface;
use Exception,Illuminate\Support\Facades\Log;

class SubjectRepository implements SubjectInterface
{
    protected $subject;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getAllAsArray()
    {
        try {
            return $this->subject->pluck('name','id')->all();
        } catch (Exception $e) {
            Log::error('Error occurred retrieving subject array .', ['exception' => $e]);
            return false;
        }

    }

}
?>
