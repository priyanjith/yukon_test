<?php

namespace App\Modules\Lectures\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use App\Modules\Lectures\Services\Interfaces\LectureInterface;
use App\Modules\Instructors\Services\Interfaces\InstructorInterface;
use App\Modules\Students\Services\Interfaces\StudentInterface;



class LectureController extends Controller
{

    protected $lecture;
    protected $instructor;
    protected $student;

    public function __construct(LectureInterface $lecture, InstructorInterface $instructor, StudentInterface $student)
    {
        $this->lecture = $lecture;
        $this->instructor = $instructor;
        $this->student = $student;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the lectures
        $lectures = $this->lecture->getAll();
        // load the view and pass the lectures
        return \View::make('lectures::index')
            ->with('lectures', $lectures);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $allInstructors = $this->instructor->getAllAsArray();
        $students = $this->student->getAll();
        $allRecurrence = [
            "DAILY" => "Daily",
            "MONTHLY" => "Monthly",
            "WEEKLY" => "Weekly",
            "CUSTOM" => "Custom"
        ];
        return view('lectures::create',compact('allInstructors', 'students', "allRecurrence"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        // validate
        $rules = array(
            'instructor_id' => 'required',
        );
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to('classes/create')
                ->withErrors($validator);
        } else {
            $instructor = $this->instructor->findById($input['instructor_id']);
            $input['subject_id'] = $instructor->subject_id;

            // store
            $lecture = $this->lecture->create($input);

            if($lecture) {
                // redirect
                Session::flash('message', 'Successfully created class!');
                return Redirect::to('classes');
            }
            // error redirect
            Session::flash('message', 'Error occurred creating a new class.!');
            return Redirect::to('classes');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $lecture = $this->lecture->findById($id);
        $allInstructors = $this->instructor->getAllAsArray();
        $students = $this->student->getAll();
        $allRecurrence = [
            "DAILY" => "Daily",
            "MONTHLY" => "Monthly",
            "WEEKLY" => "Weekly",
            "CUSTOM" => "Custom"
        ];
        // Show the page
        return view('lectures::edit', compact(
            'lecture',
            'allInstructors',
            'students',
            'allRecurrence'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();

        // validate
        $rules = array(
             'instructor_id' => 'required',
        );
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to('classes/edit/'.$id)
                ->withErrors($validator);
        } else {
            $input['id'] = $id;
            $instructor = $this->instructor->findById($input['instructor_id']);
            $input['subject_id'] = $instructor->subject_id;

            // store
            $lecture = $this->lecture->create($input);

            if($lecture) {
                // redirect
                Session::flash('message', 'Successfully updated class!');
                return Redirect::to('classes');
            }
            // error redirect
            Session::flash('message', 'Error occurred updating a new class.!');
            return Redirect::to('classes');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $result = $this->lecture->delete($id);

        if($result) {
            // redirect
            Session::flash('message', 'Successfully deleted the class!');
            return Redirect::to('classes');
        }
        // redirect
        Session::flash('message', 'Delete Failed.!');
        return Redirect::to('classes');
    }
}
