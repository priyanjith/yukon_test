<?php

namespace App\Modules\Lectures\Services\Interfaces;

interface LectureInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $Lecture_data
     *
     * @return instructor
     */
    public function create($data);

    /**
     * @param $Lecture_id
     *
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $Lecture_id
     *
     * @return instructor
     */
    public function findById($id);
}

?>