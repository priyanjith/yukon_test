<?php

namespace App\Modules\Lectures\Services\Repositories;

use App\Modules\Lectures\Models\Lecture;
use App\Modules\Lectures\Services\Interfaces\LectureInterface;
use Exception,Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class LectureRepository implements LectureInterface
{
    protected $lecture;

    public function __construct(Lecture $lecture)
    {
        $this->lecture = $lecture;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        try {
            return $this->lecture->all();;
        } catch (Exception $e) {
            Log::error('Error occurred retrieving Lectures.', ['exception' => $e]);
            return false;
        }

    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function create($data){
        try {
            DB::beginTransaction();
            if (isset($data['id'])) {
                // update
                $lecture = $this->lecture->find($data['id']);
            } else {
                //create
                $lecture = $this->lecture;
            }
            // store
            $lecture->instructor_id  = $data['instructor_id'];
            $lecture->subject_id  = $data['subject_id'];
            $lecture->start_time  = $data['start_time'];
            $lecture->end_time  = $data['end_time'];
            $lecture->recurrence  = $data['recurrence'];
            $lecture->save();

            if (isset($data['id'])) {
                $lecture->students()->sync($data['ids']);
            }else{
                $lecture->students()->attach($data['ids']);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('Error occurred adding a Lecture.',
                ['data' => $lecture, 'exception' => $e]);
            return false;
        }
        return $lecture;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById($id)
    {
        try {
            return $this->lecture->find($id);
        } catch (Exception $e) {
            Log::error('Error occurred retrieving Lecture.', ['data' => $id, 'exception' => $e]);
            return false;
        }
    }

    /**
     * @param $id
     *
     * @return mixed|void
     */
    public function delete($id)
    {
        try {
            $lecture = $this->lecture->find($id);
            if ($lecture->delete()) {
                return true;
            } else {
                throw new Exception('Delete Failed.');
            }
        } catch (Exception $e) {
            Log::error('Error occurred deleting a Lecture', ['data' => $id, 'exception' => $e]);
            return false;
        }
    }



}
?>
