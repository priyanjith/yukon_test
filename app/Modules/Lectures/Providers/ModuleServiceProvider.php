<?php

namespace App\Modules\Lectures\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'lectures');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'lectures');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'lectures');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->bind('App\Modules\Lectures\Services\Interfaces\LectureInterface', 'App\Modules\Lectures\Services\Repositories\LectureRepository');

    }
}
