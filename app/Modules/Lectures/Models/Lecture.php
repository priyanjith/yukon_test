<?php

namespace App\Modules\Lectures\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lectures';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject_id',  'instructor_id'
    ];

    public function instructor()
    {
        return $this->belongsTo('App\Modules\Instructors\Models\Instructor', 'instructor_id');
    }

    public function students()
    {
        return $this->belongsToMany('App\Modules\Students\Models\Student', 'lectures_students', 'lecture_id', 'student_id');
    }
    
}
