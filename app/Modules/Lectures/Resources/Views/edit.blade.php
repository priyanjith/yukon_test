@extends('adminlte::page')

@section('title', 'Classes Manager')

@section('content_header')
    <h1>Classes Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'classes/update/'.$lecture->id, 'method' => 'post')) }}
    <div class="form-group">
        <label for="instructor">Instructor</label>
        {!! Form::select('instructor_id',$allInstructors, isset($lecture) ? $lecture->instructor_id : 0, ['class'=>'form-control', 'placeholder'=>'Select Instructor']) !!}
        @if ( $errors->has('instructor_id') )
            <span class="text-danger">{{ $errors->first('instructor_id') }}</span>
        @endif
    </div>

    <div class="form-group">
        <label for="instructor">Start Time</label>
        <div class='input-group date' id='datetimepicker3'>
            <input type='text' name="start_time" placeholder="00:00" class="form-control" value="{{isset($lecture) ? $lecture->start_time : "00:00" }}"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                </span>
        </div>
    </div>

    <div class="form-group">
        <label for="instructor">End Time</label>
        <div class='input-group date' id='datetimepicker3'>
            <input type='text' name="end_time" placeholder="00:00" class="form-control" value="{{isset($lecture) ? $lecture->end_time : "00:00" }}"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                </span>
        </div>
    </div>

    <div class="form-group">
        <label for="recurrence">Recurrence</label>
        {!! Form::select('recurrence',$allRecurrence, isset($lecture) ? $lecture->recurrence : 0, ['class'=>'form-control', 'placeholder'=>'Select Recurrence']) !!}
    </div>

    <div class="form-group">
        <label for="students">Select Students</label>
        <table id="students-table" class="display select" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th></th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Age</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th></th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Age</th>
            </tr>
            <tbody>
            @foreach($students as $student)

                @php ($ischecked = $student->lectures->contains($lecture->id))
                <tr>
                    <td><input type="checkbox" value="{{ $student->id }}" name="ids[]" {{ ($ischecked)? "checked" : "" }} class="dt-checkboxes"></td>
                    <td>{{ $student->first_name }}</td>
                    <td>{{ $student->last_name }}</td>
                    <td>{{ $student->age }}</td>
                </tr>
            @endforeach
            </tbody>
            </tfoot>
        </table>
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <script src="http://localhost:84/new_store/assets/template/plugins/datepicker/bootstrap-datepicker.js"></script>
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#students-table').DataTable({
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                'order': [[1, 'asc']]
            });

            $('#datepickerss').datepicker({
                autoclose: true
            });

        });
    </script>
@stop