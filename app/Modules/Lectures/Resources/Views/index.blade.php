@extends('adminlte::page')

@section('title', 'Classes Manager')

@section('content_header')
    <h1>Classes Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="pull-right">
                <a href="/classes/create"><button type="button" class="btn btn-primary">Create</button></a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-10">
                    <table id="user-list" class="display responsive nowrap" width="100%">
                        <thead>
                        <tr >
                            <th class="col-md-3">Instructor</th>
                            <th class="col-md-2">Start Time</th>
                            <th class="col-md-2">End Time</th>
                            <th class="col-md-3">Recurrence</th>
                            <th class="col-md-3">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lectures as $lecture)
                            <tr style="height:60px;">
                                <td  data-th="Instructor">{{ $lecture->instructor->full_name }}</td>
                                <td class="col-md-3" data-th="Start Time">{{ $lecture->start_time }}</td>
                                <td class="col-md-3" data-th="End Time">{{ $lecture->end_time }}</td>
                                <td class="col-md-3" data-th="Recurrence">{{ $lecture->recurrence }}</td>
                                <td class="col-md-3" data-th="Actions">
                                    <div class="btn-group">
                                        <a href="/classes/edit/{{ $lecture->id }}">
                                            <button type="button" class="btn btn-info">Update</button>
                                        </a>
                                        <a href="/classes/delete/{{ $lecture->id }}">
                                            <button type="button" class="btn btn-danger">Remove</button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

@section('js')
    <script>
        $(document).ready(function() {

            $('#user-list').DataTable();
        } );
    </script>
@stop