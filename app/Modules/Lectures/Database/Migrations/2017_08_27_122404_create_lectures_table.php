<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectures', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('subject_id')->notnull()->unsigned();
            $table->integer('instructor_id')->notnull()->unsigned();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->string('recurrence',100)->nullable();
            $table->timestamps();
        });

        Schema::table('lectures', function($table) {
            $table->foreign('instructor_id')->references('id')->on('instructors')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lectures');
    }
}
