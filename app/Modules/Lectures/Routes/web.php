<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'classes'], function () {
    Route::get('/', 'LectureController@index')->name('classes');
    Route::get('/create', 'LectureController@create')->name('classes.create');
    Route::post('/store', 'LectureController@store')->name('classes.store');
    Route::get('/edit/{id}', 'LectureController@edit')->name('classes.edit');
    Route::post('/update/{id}', 'LectureController@update')->name('classes.update');
    Route::get('/delete/{id}', 'LectureController@destroy')->name('classes.delete');
});
