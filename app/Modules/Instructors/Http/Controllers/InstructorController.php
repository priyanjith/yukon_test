<?php

namespace App\Modules\Instructors\Http\Controllers;

use App\Modules\Subjects\Services\Interfaces\SubjectInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use App\Modules\Instructors\Services\Interfaces\InstructorInterface;

class InstructorController extends Controller
{

    protected $instructor;
    protected $subject;

    public function __construct(InstructorInterface $instructor, SubjectInterface $subject)
    {
        $this->instructor = $instructor;
        $this->subject = $subject;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the instructors
        $instructors = $this->instructor->getAll();
        // load the view and pass the instructors
        return \View::make('instructors::index')
            ->with('instructors', $instructors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $allSubjects = $this->subject->getAllAsArray();
        return view('instructors::create',compact('allSubjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        // validate
        $rules = array(
            'full_name' => 'required',
        );
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to('instructors/create')
                ->withErrors($validator);
        } else {
            $instructors = $this->instructor->create($input);
            if($instructors) {
                // redirect success
                Session::flash('message', 'Successfully created instructor!');
                return Redirect::to('instructors');
            }
            // error redirect
            Session::flash('message', 'Error occurred creating a new instructor.!');
            return Redirect::to('instructors');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $instructor = $this->instructor->findById($id);
        $allSubjects = $this->subject->getAllAsArray();

        // Show the page
        return view('instructors::edit', compact(
            'instructor',
            'allSubjects'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        // validate
        $rules = array(
            'full_name' => 'required',
        );
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to('instructors/edit/'.$id)
                ->withErrors($validator);
        } else {
            // this is an update set the id
            $input['id'] = $id;
            $instructors = $this->instructor->create($input);
            if($instructors) {
                // redirect success
                Session::flash('message', 'Successfully updated instructor!');
                return Redirect::to('instructors');
            }
            // error redirect
            Session::flash('message', 'Error occurred updating a new instructor.!');
            return Redirect::to('instructors');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $result = $this->instructor->delete($id);

        if($result) {
            // redirect
            Session::flash('message', 'Successfully deleted the instructor!');
            return Redirect::to('instructors');
        }
        // redirect
        Session::flash('message', 'Delete Failed.!');
        return Redirect::to('instructors');
    }
}
