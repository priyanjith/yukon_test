@extends('adminlte::page')

@section('title', 'Instructor Manager')

@section('content_header')
    <h1>Instructor Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'instructors/update/'.$instructor->id, 'method' => 'post')) }}

    <div class="form-group">
        <label for="subject">Subject</label>
        {!! Form::select('subject_id',$allSubjects, isset($instructor) ? $instructor->subject_id : 0  , ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
    </div>
    <div class="form-group">
        <label for="full_name">Full Name</label>
        <input type="text" name="full_name" class="form-control"  placeholder="Full Name" value="{{ Input::old('name', isset($instructor) ? $instructor->full_name : null) }}">
        @if ( $errors->has('full_name') )
            <span class="text-danger">{{ $errors->first('full_name') }}</span>
        @endif
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea type="text" name="description" class="form-control"  placeholder="Description">{{ Input::old('description', isset($instructor) ? $instructor->description : null) }}</textarea>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop