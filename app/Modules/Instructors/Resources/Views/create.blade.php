@extends('adminlte::page')

@section('title', 'Instructor Manager')

@section('content_header')
    <h1>Instructor Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'instructors/store', 'method' => 'post')) }}
        <div class="form-group">
            <label for="subject">Subject</label>
            {!! Form::select('subject_id',$allSubjects, old('subject_id'), ['class'=>'form-control', 'placeholder'=>'Select Subject']) !!}
            @if ( $errors->has('subject_id') )
                <span class="text-danger">{{ $errors->first('subject_id') }}</span>
            @endif
        </div>
        <div class="form-group">
            <label for="full_name">Full Name</label>
            <input type="text" name="full_name" class="form-control"  placeholder="Full Name">
            @if ( $errors->has('full_name') )
                <span class="text-danger">{{ $errors->first('full_name') }}</span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea type="text" name="description" class="form-control"  placeholder="Description"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop