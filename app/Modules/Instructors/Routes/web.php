<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'instructors'], function () {
    Route::get('/', 'InstructorController@index')->name('instructors');
    Route::get('/create', 'InstructorController@create')->name('instructors.create');
    Route::post('/store', 'InstructorController@store')->name('instructors.store');
    Route::get('/edit/{id}', 'InstructorController@edit')->name('instructors.edit');
    Route::post('/update/{id}', 'InstructorController@update')->name('instructors.update');
    Route::get('/delete/{id}', 'InstructorController@destroy')->name('instructors.delete');
});
