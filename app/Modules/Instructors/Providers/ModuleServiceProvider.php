<?php

namespace App\Modules\Instructors\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'instructors');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'instructors');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'instructors');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->bind('App\Modules\Instructors\Services\Interfaces\InstructorInterface', 'App\Modules\Instructors\Services\Repositories\InstructorRepository');

    }
}
