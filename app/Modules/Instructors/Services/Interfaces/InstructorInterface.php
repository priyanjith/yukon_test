<?php

namespace App\Modules\Instructors\Services\Interfaces;

interface InstructorInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $Instructor_data
     *
     * @return instructor
     */
    public function create($data);

    /**
     * @param $Instructor_id
     *
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $Instructor_id
     *
     * @return instructor
     */
    public function findById($id);

    /**
     * @return instructors array
     */
    public function getAllAsArray();
}

?>