<?php

namespace App\Modules\Instructors\Services\Repositories;

use App\Modules\Instructors\Models\Instructor;
use App\Modules\Instructors\Services\Interfaces\InstructorInterface;
use Exception,Illuminate\Support\Facades\Log;

class InstructorRepository implements InstructorInterface
{
    protected $instructor;

    public function __construct(Instructor $instructor)
    {
        $this->instructor = $instructor;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        try {
            return $this->instructor->all();;
        } catch (Exception $e) {
            Log::error('Error occurred retrieving Instructors.', ['exception' => $e]);
            return false;
        }

    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function create($data){
        try {
            if (isset($data['id'])) {
                // update
                $instructor = $this->instructor->find($data['id']);
            } else {
                //create
                $instructor = $this->instructor;
            }
            // store
            $instructor->full_name  = $data['full_name'];
            $instructor->subject_id  = $data['subject_id'];
            $instructor->description  = $data['description'];
            $instructor->save();

        } catch (Exception $e) {
            Log::error('Error occurred adding a Instructor.',
                ['data' => $instructor, 'exception' => $e]);
            return false;
        }
        return $instructor;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById($id)
    {
        try {
            return $this->instructor->find($id);
        } catch (Exception $e) {
            Log::error('Error occurred retrieving Instructor.', ['data' => $id, 'exception' => $e]);
            return false;
        }
    }

    /**
     * @param $id
     *
     * @return mixed|void
     */
    public function delete($id)
    {
        try {
            $instructor = $this->instructor->find($id);
            if ($instructor->delete()) {
                return true;
            } else {
                throw new Exception('Delete Failed.');
            }
        } catch (Exception $e) {
            Log::error('Error occurred deleting a Instructor', ['data' => $id, 'exception' => $e]);
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getAllAsArray()
    {
        try {
            return $this->instructor->pluck('full_name','id')->all();
        } catch (Exception $e) {
            Log::error('Error occurred retrieving subject array .', ['exception' => $e]);
            return false;
        }

    }



}
?>
