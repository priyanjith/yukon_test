<?php

namespace App\Modules\Instructors\Models;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'instructors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'description', 'subject_id'
    ];

    public function subject()
    {
        return $this->belongsTo('App\Modules\Subjects\Models\Subject', 'subject_id');
    }

    public function lectures()
    {
        return $this->hasMany('App\Modules\Lectures\Models\Lecture');
    }
}
