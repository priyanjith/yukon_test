<?php

namespace App\Modules\Students\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Session;
use Response;
use App\Http\Controllers\Controller;
use App\Modules\Students\Services\Interfaces\StudentInterface ;

class StudentController extends Controller
{

    protected $student;

    public function __construct(StudentInterface $student)
    {
        $this->student = $student;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the students
        $students = $this->student->getAll();
        // load the view and pass the students
        return \View::make('students::index')
            ->with('students', $students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('students::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        // validate
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'grade' => 'required',
            'age' => 'required',
        );
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to('students/create')
                ->withErrors($validator);
        } else {
            // store
            $student= $this->student->create($input);
            if($student) {
                // redirect
                Session::flash('message', 'Successfully created student!');
                return Redirect::to('students');
            }
            // error redirect
            Session::flash('message', 'Error occurred creating a new student.!');
            return Redirect::to('students');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $student = $this->student->findById($id);
        // Show the page
        return view('students::edit', compact(
            'student'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        // validate
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'grade' => 'required',
            'age' => 'required',
        );
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to('students/edit/'.$id)
                ->withErrors($validator);
        } else {
            // this is an update set the id
            $input['id'] = $id;
            $students = $this->student->create($input);
            if($students) {
                // redirect success
                Session::flash('message', 'Successfully updated student!');
                return Redirect::to('students');
            }
            // error redirect
            Session::flash('message', 'Error occurred updating a new student.!');
            return Redirect::to('students');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $result = $this->student->delete($id);

        if($result) {
            // redirect
            Session::flash('message', 'Successfully deleted the student!');
            return Redirect::to('students');
        }
        // redirect
        Session::flash('message', 'Delete Failed.!');
        return Redirect::to('students');
    }

    public function  allStudents(){
        $students = $this->student->getAll();
        return Response::json($students->toJson());
    }
}
