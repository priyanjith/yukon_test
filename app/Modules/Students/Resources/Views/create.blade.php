@extends('adminlte::page')

@section('title', 'Student Manager')

@section('content_header')
    <h1>Student Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'students/store', 'method' => 'post')) }}

        <div class="form-group">
            <label for="last_name">First Name</label>
            <input type="text" name="first_name" class="form-control"  placeholder="First Name">
            @if ( $errors->has('first_name') )
                <span class="text-danger">{{{ $errors->first('first_name') }}}</span>
            @endif
        </div>
        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" name="last_name" class="form-control"  placeholder="Last Name">
            @if ( $errors->has('last_name') )
                <span class="text-danger">{{{ $errors->first('last_name') }}}</span>
            @endif
        </div>
        <div class="form-group">
            <label for="grade">Grade</label>
            <input type="text" name="grade" class="form-control"  placeholder="Grade">
            @if ( $errors->has('grade') )
                <span class="text-danger">{{{ $errors->first('grade') }}}</span>
            @endif
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="number" name="age" class="form-control"  placeholder="Grade">
            @if ( $errors->has('age') )
                <span class="text-danger">{{{ $errors->first('age') }}}</span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea type="text" name="description" class="form-control"  placeholder="Description"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop