@extends('adminlte::page')

@section('title', 'Student Manager')

@section('content_header')
    <h1>Student Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="pull-right">
                <a href="/students/create"><button type="button" class="btn btn-primary">Create</button></a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table id="user-list" class="display responsive nowrap" width="100%">
                        <thead>
                        <tr >
                            <th class="col-md-3">First Name</th>
                            <th class="col-md-3">Last Name</th>
                            <th class="col-md-1">Age</th>
                            <th class="col-md-2">Grade</th>
                            <th class="col-md-3">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                            <tr style="height:60px;">
                                <td class="col-md-3" data-th="First Name">{{ $student->first_name }}</td>
                                <td class="col-md-3" data-th="Last Name">{{ $student->last_name }}</td>
                                <td class="col-md-1" data-th="Age">{{ $student->age }}</td>
                                <td class="col-md-2" data-th="Grade">{{ $student->grade }}</td>
                                <td class="col-md-3" data-th="Actions">
                                    <div class="btn-group">
                                        <a href="/students/edit/{{ $student->id }}">
                                            <button type="button" class="btn btn-info">Update</button>
                                        </a>
                                        <a href="/students/delete/{{ $student->id }}">
                                            <button type="button" class="btn btn-danger">Remove</button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

@section('js')
    <script>
        $(document).ready(function() {

            $('#user-list').DataTable();
        } );
    </script>
@stop