<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'students'], function () {
    Route::get('/', 'StudentController@index')->name('students');
    Route::get('/create', 'StudentController@create')->name('students.create');
    Route::post('/store', 'StudentController@store')->name('students.store');
    Route::get('/edit/{id}', 'StudentController@edit')->name('students.edit');;
    Route::post('/update/{id}', 'StudentController@update')->name('students.update');
    Route::get('/delete/{id}', 'StudentController@destroy')->name('students.delete');
    Route::get('/all', 'StudentController@allStudents')->name('student.all');
});

