<?php

namespace App\Modules\Students\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'students';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'grade', 'description', 'age'
    ];

    public function lectures()
    {
        return $this->belongsToMany('App\Modules\Lectures\Models\Lecture', 'lectures_students', 'student_id', 'lecture_id');
    }
    
}
