<?php

namespace App\Modules\Students\Services\Interfaces;

interface StudentInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $Student_data
     *
     * @return instructor
     */
    public function create($data);

    /**
     * @param $Student_id
     *
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $Student_id
     *
     * @return instructor
     */
    public function findById($id);
}

?>