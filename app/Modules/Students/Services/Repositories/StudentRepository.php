<?php

namespace App\Modules\Students\Services\Repositories;

use App\Modules\Students\Models\Student;
use App\Modules\Students\Services\Interfaces\StudentInterface;
use Exception,Illuminate\Support\Facades\Log;


class StudentRepository implements StudentInterface
{
    protected $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        try {
            return $this->student->all();;
        } catch (Exception $e) {
            Log::error('Error occurred retrieving Students.', ['exception' => $e]);
            return false;
        }

    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function create($data){
        try {
            if (isset($data['id'])) {
                // update
                $student = $this->student->find($data['id']);
            } else {
                //create
                $student = $this->student;
            }
            // store
            $student->first_name  = $data['first_name'];
            $student->last_name  = $data['last_name'];
            $student->age  = $data['age'];
            $student->description  = $data['description'];
            $student->grade  = $data['grade'];
            $student->save();

        } catch (Exception $e) {
            Log::error('Error occurred adding a Student.',
                ['data' => $student, 'exception' => $e]);
            return false;
        }
        return $student;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findById($id)
    {
        try {
            return $this->student->find($id);
        } catch (Exception $e) {
            Log::error('Error occurred retrieving Student.', ['data' => $id, 'exception' => $e]);
            return false;
        }
    }

    /**
     * @param $id
     *
     * @return mixed|void
     */
    public function delete($id)
    {
        try {
            $student = $this->student->find($id);
            if ($student->delete()) {
                return true;
            } else {
                throw new Exception('Delete Failed.');
            }
        } catch (Exception $e) {
            Log::error('Error occurred deleting a Student', ['data' => $id, 'exception' => $e]);
            return false;
        }
    }



}
?>
