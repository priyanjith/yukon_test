<?php

namespace App\Modules\Students\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'students');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'students');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'students');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->bind('App\Modules\Students\Services\Interfaces\StudentInterface', 'App\Modules\Students\Services\Repositories\StudentRepository');

    }
}
